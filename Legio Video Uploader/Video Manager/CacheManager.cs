﻿using System;

namespace LegioVideoUploader.VideoManager {

    internal class CacheManager {
        private System.Threading.AutoResetEvent attesa;
        private System.Threading.Thread cacheCheckThread;
        private static CacheManager istance;
        private bool lavora;
        private log4net.ILog logger = log4net.LogManager.GetLogger (typeof (CacheManager));

        private CacheManager () {
            cacheCheckThread = new System.Threading.Thread (new System.Threading.ThreadStart (this.controllaCache));
            cacheCheckThread.Priority = System.Threading.ThreadPriority.Lowest;
            istance = this;
            attesa = new System.Threading.AutoResetEvent (false);
        }

        public static CacheManager Istance {
            get {
                if (istance == null)
                    istance = new CacheManager ();
                return istance;
            }
        }

        public Database.CacheData getVideo (String nome, String url) {
            return get (nome, url, Database.TipoLinkEnum.youtube);
        }

        public Database.CacheData getImage (String nome, String url) {
            return get (nome, url, Database.TipoLinkEnum.imgur);
        }

        public Database.CacheData getAudio (String nome, String url) {
            return get (nome, url, Database.TipoLinkEnum.mega);
        }

        private Database.CacheData get (String nome, String url, Database.TipoLinkEnum tipo) {
            SQLite.SQLiteConnection conn = (SQLite.SQLiteConnection) App.Current.Properties["Database"];
            var query = conn.Table<Database.CacheData> ().Where (v => v.Url.Equals (url) && v.Nome.Equals (nome) && v.TipoLink == tipo);
            Database.CacheData video;
            if (query.Count () > 0) {
                video = query.First ();
            } else {
                video = new Database.CacheData ();
                video.Nome = nome;
                video.Url = url;
                video.UltimoUtilizzo = DateTime.Now;
                video.TipoLink = tipo;
                conn.Insert (video);
            }

            return video;
        }

        #region Funzioni Thread Background

        public void Start () {
            logger.Info ("Avvio Thread Gestione Cache");
            lavora = true;
            cacheCheckThread.Start ();
        }

        public void Stop () {
            logger.Info ("Fermo Thread Gestione Cache");
            lavora = false;
            attesa.Set ();
            cacheCheckThread.Join ();
            logger.Info ("Thread Gestione Cache Fermato");
        }

        private void controllaCache () {
            //log4net.ILog loggerThread = log4net.LogManager.GetLogger (typeof (CacheManager));
            logger.Info ("Thread Gestione Cache Avviato");

            SQLite.SQLiteConnection conn = (SQLite.SQLiteConnection) App.Current.Properties["Database"];
            while (lavora) {
                Impostazioni.Impostazioni imp = (Impostazioni.Impostazioni) App.Current.Properties["Impostazioni"];
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo (Utility.getCacheFolder ());
                long peso = dir.ByteSize ();
                var query = conn.Table<Database.CacheData> ().Where (v => v.Scaricato == true).OrderBy (v => v.UltimoUtilizzo);
                int index = 0;
                while (index < query.Count () && peso > imp.Generale.PesoCache) {
                    Database.CacheData video = query.ElementAt (index);
                    try {
                        if (!video.InUso) {
                            System.IO.File.Delete (video.FileName);
                            video.Scaricato = false;
                            video.FileName = null;
                            conn.Update (video);
                        }
                    } catch (Exception e) {
                        logger.Error (String.Format ("Errore nell'eliminare il file {0} ({1})", video.Nome, video.FileName), e);
                    }
                    peso = dir.ByteSize ();
                    index++;
                }
                //Controllo file presenti in cartella cache
                System.IO.FileInfo[] files = dir.GetFiles ();
                foreach (System.IO.FileInfo file in files) {
                    query = conn.Table<Database.CacheData> ().Where (v => v.FileName.Equals (file.FullName));
                    if (query.Count () == 0)
                        file.Delete ();
                }

                attesa.WaitOne (10 * 60 * 1000);
            }
        }

        #endregion Funzioni Thread Background
    }
}
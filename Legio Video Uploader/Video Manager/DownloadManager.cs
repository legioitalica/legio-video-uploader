﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.VideoManager {

    internal class DownloadManager {
        private static DownloadManager _istance;
        private System.Threading.Thread threadGestioneDownload;
        private System.Threading.AutoResetEvent attesa;
        private bool isRunning;
        private Queue<KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso>> downloadInCoda;

        private log4net.ILog logger = log4net.LogManager.GetLogger (typeof (DownloadManager));

        public event EventHandler<DownloadEventArgs> DownloadFinito;

        private delegate void UpdateProgressCallback (double progress, KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso> kvVideo);

        private DownloadManager () {
            _istance = this;
            threadGestioneDownload = new System.Threading.Thread (new System.Threading.ThreadStart (this.Run));
            downloadInCoda = new Queue<KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso>> ();
            isRunning = false;
            DownloadCount = 0;
            attesa = new System.Threading.AutoResetEvent (false);
        }

        public static DownloadManager Istance {
            get {
                if (_istance == null)
                    _istance = new DownloadManager ();

                return _istance;
            }
        }

        public void Download (Database.CacheData video, out UI.Componenti.ContolloUtenteProgresso controllo) {
            controllo = App.Current.Dispatcher.Invoke (delegate {
                UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                tmp.Progresso.Maximum = 100;
                return tmp;
            });
            lock (downloadInCoda) {
                downloadInCoda.Enqueue (new KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso> (video, controllo));
            }
            attesa.Set ();
        }

        public int DownloadCount { get; set; }

        public void Start () {
            logger.Info ("Avvio Thread Download");
            isRunning = true;
            threadGestioneDownload.Start ();
        }

        public void Stop () {
            logger.Info ("Fermo Thread Download");
            isRunning = false;
            attesa.Set ();
            threadGestioneDownload.Join ();
            logger.Info ("Thread Download Terminato");
        }

        private void Run () {
            //log4net.ILog loggerThread = log4net.LogManager.GetLogger (typeof (CacheManager));

            logger.Info ("Thread Download Avviato");
            while (isRunning) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = App.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                SQLite.SQLiteConnection conn = (SQLite.SQLiteConnection) App.Current.Properties["Database"];

                lock (downloadInCoda) {
                    while (DownloadCount <= imp.Generale.MaxDownloadSimultanei && downloadInCoda.Count > 0) {
                        KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso> kvDownload;
                        lock (downloadInCoda) {
                            kvDownload = downloadInCoda.Dequeue ();
                        }
                        Database.CacheData download = kvDownload.Key;
                        String nome = System.IO.Path.Combine (Utility.getCacheFolder (), System.IO.Path.GetRandomFileName ());
                        switch (download.TipoLink) {
                            case Database.TipoLinkEnum.youtube:
                                IEnumerable<YoutubeExtractor.VideoInfo> videoInfos = YoutubeExtractor.DownloadUrlResolver.GetDownloadUrls (download.Url);
                                YoutubeExtractor.VideoInfo v = videoInfos.OrderByDescending (info => info.Resolution).First (info => info.VideoType == YoutubeExtractor.VideoType.Mp4);
                                if (v.RequiresDecryption) {
                                    YoutubeExtractor.DownloadUrlResolver.DecryptDownloadUrl (v);
                                }

                                nome += v.VideoExtension;

                                download.FileName = nome;

                                var videoDownloader = new YoutubeExtractor.VideoDownloader (v, download.FileName);
                                videoDownloader.DownloadProgressChanged += (sender, args) => {
                                    kvDownload.Value.Progresso.Dispatcher.Invoke (new UpdateProgressCallback (this.updateProgress), new Object[] { args.ProgressPercentage, kvDownload });
                                };

                                videoDownloader.DownloadFinished += (object sender, EventArgs e) => {
                                    VideoDownloader_DownloadFinished (sender, e, download, kvDownload.Value);
                                };

                                System.Threading.ThreadPool.QueueUserWorkItem (new System.Threading.WaitCallback (delegate (object state) {
                                    //log4net.ILog loggerThreadPool = log4net.LogManager.GetLogger (typeof (CacheManager));
                                    kvDownload.Value.Start ();
                                    logger.InfoFormat ("Avvio Download di {0} ({1})", download.Nome, download.Url);
                                    videoDownloader.Execute ();
                                }));
                                DownloadCount++;
                                break;

                            case Database.TipoLinkEnum.imgur:
                                kvDownload.Value.Start ();
                                nome += download.Url.Substring (download.Url.LastIndexOf ('.'));
                                download.FileName = nome;
                                kvDownload.Value.InBytes = true;
                                logger.InfoFormat ("Avvio Download di {0} ({1})", download.Nome, download.Url);
                                System.Net.WebClient wc = new System.Net.WebClient ();
                                wc.DownloadProgressChanged += (sender, args) => {
                                    kvDownload.Value.Dispatcher.Invoke (new Action (() => {
                                        kvDownload.Value.Maximum = args.TotalBytesToReceive;
                                        kvDownload.Value.Value = args.BytesReceived;
                                    }));
                                };
                                wc.DownloadFileCompleted += (sender, args) => {
                                    logger.InfoFormat ("Download di {0} finito", download.Nome);
                                    download.NumeroDownload++;
                                    download.Scaricato = true;
                                    conn.Update (download);
                                    DownloadCount--;
                                    kvDownload.Value.Dispatcher.Invoke (new Action (() => { kvDownload.Value.Stop (); }));
                                    kvDownload.Value.Dispatcher.Invoke (new Action (() => { RaiseDownloadFinito (download); }));
                                    attesa.Set ();
                                };
                                wc.DownloadFileAsync (new Uri (download.Url), download.FileName);

                                DownloadCount++;
                                break;

                            case Database.TipoLinkEnum.mega:
                                CG.Web.MegaApiClient.MegaApiClient client = new CG.Web.MegaApiClient.MegaApiClient ();
                                client.LoginAnonymous ();

                                var fileInfo = client.GetNodeFromLink (new Uri (download.Url));
                                String ext = System.IO.Path.GetExtension (fileInfo.Name);

                                nome += ext;
                                download.FileName = nome;

                                System.Threading.ThreadPool.QueueUserWorkItem (new System.Threading.WaitCallback (async delegate (object state) {
                                    //log4net.ILog loggerThreadPool = log4net.LogManager.GetLogger (typeof (CacheManager));
                                    kvDownload.Value.Start ();
                                    logger.InfoFormat ("Avvio Download di {0} ({1})", download.Nome, download.Url);
                                    await client.DownloadFileAsync (new Uri (download.Url), nome, new Progress<double> (new Action<double> ((double progresso) => {
                                        kvDownload.Value.Progresso.Dispatcher.Invoke (new Action (() => {
                                            kvDownload.Value.Progresso.Value = progresso;
                                        }));
                                    })));
                                    logger.InfoFormat ("Download di {0} finito", download.Nome);
                                    download.NumeroDownload++;
                                    download.Scaricato = true;
                                    conn.Update (download);
                                    DownloadCount--;
                                    kvDownload.Value.Dispatcher.Invoke (new Action (() => { kvDownload.Value.Stop (); }));
                                    kvDownload.Value.Dispatcher.Invoke (new Action (() => { RaiseDownloadFinito (download); }));
                                    attesa.Set ();
                                }));
                                DownloadCount++;
                                break;

                            default:
                                break;
                        }
                    }
                }
                attesa.WaitOne ();
            }
        }

        private void updateProgress (double value, KeyValuePair<Database.CacheData, UI.Componenti.ContolloUtenteProgresso> kvVideo) {
            kvVideo.Value.Progresso.Value = value;
        }

        private void VideoDownloader_DownloadFinished (object sender, EventArgs e, Database.CacheData video, UI.Componenti.ContolloUtenteProgresso progresso) {
            logger.InfoFormat ("Download di {0} finito", video.Nome);
            video.NumeroDownload++;
            video.Scaricato = true;
            SQLite.SQLiteConnection conn = (SQLite.SQLiteConnection) App.Current.Properties["Database"];
            conn.Update (video);
            DownloadCount--;
            progresso.Stop ();
            RaiseDownloadFinito (video);
            attesa.Set ();
        }

        private void RaiseDownloadFinito (Database.CacheData video) {
            if (DownloadFinito != null) {
                DownloadFinito (this, new DownloadEventArgs (video));
            }
        }
    }
}
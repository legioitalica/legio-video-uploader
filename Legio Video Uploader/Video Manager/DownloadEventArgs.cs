﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.VideoManager {

    internal class DownloadEventArgs : EventArgs {

        public DownloadEventArgs (Database.CacheData video) {
            Video = video;
        }

        public Database.CacheData Video { get; private set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace LegioVideoUploader.Database {

    public enum TipoLinkEnum {
        youtube,
        mega,
        imgur
    }

    internal class CacheData {

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [NotNull]
        public String Nome { get; set; }

        [NotNull, Indexed]
        public String Url { get; set; }

        public bool Scaricato { get; set; }
        public String FileName { get; set; }

        [NotNull]
        public DateTime UltimoUtilizzo { get; set; }

        public bool InUso { get; set; }
        public uint NumeroUtilizzi { get; set; }
        public uint NumeroDownload { get; set; }
        public TipoLinkEnum TipoLink { get; set; }
    }
}
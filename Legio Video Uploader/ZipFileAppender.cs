﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader {

    public class ZipFileAppender : log4net.Appender.RollingFileAppender {

        protected override void AdjustFileBeforeAppend () {
            base.AdjustFileBeforeAppend ();
            if (System.IO.File.Exists (Utility.getLogFileName () + ".1")) { // zip the file
                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile (Utility.getLogZipFileName ())) {
                    long elementiPresenti = zip.LongCount ();
                    String newName = System.IO.Path.Combine (System.IO.Path.GetDirectoryName (Utility.getLogFileName ()), System.IO.Path.GetFileNameWithoutExtension (Utility.getLogFileName ())) + "." + elementiPresenti + ".log";
                    System.IO.File.Move (Utility.getLogFileName () + ".1", newName);
                    zip.AddFile (Utility.getLogFileName () + "." + elementiPresenti, "");
                    zip.Save ();
                    System.IO.File.Delete (newName);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader {

    internal static partial class ApiKeys {
        public static String PayPalLink { get; private set; }

        public static String YouTubeApiKey { get; private set; }

        public static String ImgurClientID { get; private set; }
        public static String ImgurClientSecret { get; private set; }
        public static String BitbucketConsumerKey { get; private set; }
        public static String BitbucketConsumerSecretKey { get; private set; }
        public static String BattleNetKey { get; private set; }
        public static String BattleNetSecret { get; private set; }
    }
}
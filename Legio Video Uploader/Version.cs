namespace LegioVideoUploader {

    internal class Version {
        public static readonly System.Version number = new System.Version (1,0,0,531);
        public const string numberWithRevString = "1.0.0.531";
        public const string numberString = "1.0.031";
        public const string branchName = "develop";
        public const string branchSha1 = "688aed3";
        public const string branchRevCount = "12";
        public const string informational = "1.0.0.531 [ 688aed3 ]";
        public const string informationalFull = "1.0.0.531 [ 688aed3 ] /'develop':12";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using log4net.Core;

namespace LegioVideoUploader {

    internal class DataGridAppender : log4net.Appender.AppenderSkeleton {
        private System.Windows.Controls.DataGrid _grid;

        private delegate void InsertCallback (Object item);

        protected override void Append (LoggingEvent loggingEvent) {
            if (_grid == null) {
                //LegioVideoUploader.UI.Finestre.MainWindow mw = (LegioVideoUploader.UI.Finestre.MainWindow) App.Current.Dispatcher.Invoke (delegate () { return App.Current.MainWindow; });
                if (System.Threading.Thread.CurrentThread == Application.Current.Dispatcher.Thread) {
                    LegioVideoUploader.UI.Finestre.MainWindow mw = (LegioVideoUploader.UI.Finestre.MainWindow) Application.Current.MainWindow;
                    if (mw != null)
                        _grid = mw.ListaLog;
                    else
                        return;
                } else
                    return;
            }

            System.Windows.Media.SolidColorBrush colore = System.Windows.Media.Brushes.White;

            if (loggingEvent.Level == Level.Warn)
                colore = System.Windows.Media.Brushes.Gold;
            else if (loggingEvent.Level == Level.Info)
                colore = System.Windows.Media.Brushes.LightGreen;
            else if (loggingEvent.Level == Level.Error || loggingEvent.Level == Level.Fatal)
                colore = System.Windows.Media.Brushes.Red;

            _grid.Dispatcher.Invoke (new InsertCallback (this.inserisciLinea), new Object[] { new {
                Data = loggingEvent.TimeStamp.ToString (),
                Livello = loggingEvent.Level.ToString (),
                Thread = loggingEvent.ThreadName,
                Messaggio = RenderLoggingEvent (loggingEvent).TrimEnd (System.Environment.NewLine.ToCharArray ()).Trim (),
                Sender = loggingEvent.LoggerName,
                Sfondo = colore
            } });
        }

        private void inserisciLinea (Object item) {
            _grid.Items.Add (item);
            var border = System.Windows.Media.VisualTreeHelper.GetChild (_grid, 0) as System.Windows.Controls.Decorator;
            if (border != null) {
                var scroll = border.Child as System.Windows.Controls.ScrollViewer;
                if (scroll != null) scroll.ScrollToEnd ();
            }
        }
    }
}
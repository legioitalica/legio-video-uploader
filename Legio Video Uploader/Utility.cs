﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	utility.cs
//
// summary:	Classe statica con funzioni di aiuto
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;

namespace LegioVideoUploader {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Classe con funzioni di aiuto. </summary>
    ///
    /// <remarks>   Luca, 03/02/2017. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    internal static class Utility {

        /// <summary>   Suffissi di peso. </summary>
        private static readonly String[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene la cartella dell'applicazione in <b>%appdata%.</b>
        ///             <para>Se la cartella non è presente viene creata</para>
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   La cartella dell'applicazione in %appdata%. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getAppDataFolder () {
            String path = System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), @"Legio Video Uploader");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene la cartella di cache. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Full path della cartella di cache. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getCacheFolder () {
            String path = System.IO.Path.Combine (getAppDataFolder (), "cache");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        public static String getTempFolder () {
            String path = System.IO.Path.Combine (getAppDataFolder (), "temp");
            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return path;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del file di configurazione. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del file di configurazione completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getConfigFileName () {
            return System.IO.Path.Combine (getAppDataFolder (), @"config.json");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del database. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del database completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getDatabaseFileName () {
            return System.IO.Path.Combine (getAppDataFolder (), @"data.db");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il valore con dimensione aggiunta. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="value">
        ///     Valore in byte da convertire.
        /// </param>
        /// <param name="decimalPlaces">
        ///     Numero di posizioni decimali.
        /// </param>
        ///
        /// <returns>   Stringa in formato {valore} {dimensione}. (es 1.25 GB) </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String sizeSuffix (Int64 value, int decimalPlaces = 1) {
            if (value < 0) { return "-" + sizeSuffix (-value); }
            if (value == 0) { return "0.0 bytes"; }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int) Math.Log (value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag)
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal) value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round (adjustedSize, decimalPlaces) >= 1000) {
                mag += 1;
                adjustedSize /= 1024;
            }

            return String.Format ("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome del file di log. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome del file di log completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getLogFileName () {
            String path = System.IO.Path.Combine (getAppDataFolder (), @"log");

            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return System.IO.Path.Combine (path, "LegioVideoUploader.log");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene il nome dell'archivio zip dei file di log. </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <returns>   Il nome dell'archivio zip dei file di log completo di path. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String getLogZipFileName () {
            String path = System.IO.Path.Combine (getAppDataFolder (), @"log");

            if (!System.IO.Directory.Exists (path))
                System.IO.Directory.CreateDirectory (path);
            return System.IO.Path.Combine (path, "LegioVideoUploader.log.zip");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.Controls.RichTextBox che aggiunge un testo alla
        ///     fine.
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="box">
        ///     Il System.Windows.Controls.RichTextBox a cui aggiungere il testo.
        /// </param>
        /// <param name="text">
        ///     Testo da aggiungere.
        /// </param>
        /// <param name="color">
        ///     Colore del testo.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void AppendText (this System.Windows.Controls.RichTextBox box, String text, System.Windows.Media.SolidColorBrush color) {
            System.Windows.Media.BrushConverter bc = new System.Windows.Media.BrushConverter ();
            System.Windows.Documents.TextRange tr = new System.Windows.Documents.TextRange (box.Document.ContentEnd, box.Document.ContentEnd);
            tr.Text = text;
            tr.ApplyPropertyValue (System.Windows.Documents.TextElement.BackgroundProperty, color);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Collections.Specialized.OrderedDictionary che trova
        ///     l'indice di una specifica chiave
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="dic">
        ///     OrderedDictionary in cui cercare.
        /// </param>
        /// <param name="key">
        ///     Chiave da cercare.
        /// </param>
        ///
        /// <returns>   The zero-based index of the found key, or -1 if no match was found. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static int indexOfKey (this System.Collections.Specialized.OrderedDictionary dic, Object key) {
            int index = 0;
            Object[] keyCollection = new Object[dic.Count];
            dic.Keys.CopyTo (keyCollection, 0);
            for (; index < keyCollection.Length; index++)
                if (keyCollection[index].Equals (key))
                    return index;
            return -1;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Un metodo di estensione per System.IO.DirectoryInfo che restituisce il peso in byte
        ///             della cartella.
        /// </summary>
        ///
        /// <remarks>   Luca, 03/02/2017. </remarks>
        ///
        /// <param name="dir">
        ///     Cartella da pesare.
        /// </param>
        ///
        /// <returns>   Peso della cartella in byte. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static long ByteSize (this System.IO.DirectoryInfo dir) {
            if (!dir.Exists)
                return 0;
            long size = 0;
            // Add file sizes.
            System.IO.FileInfo[] fis = dir.GetFiles ();
            foreach (System.IO.FileInfo fi in fis) {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            System.IO.DirectoryInfo[] dis = dir.GetDirectories ();
            foreach (System.IO.DirectoryInfo di in dis) {
                size += di.ByteSize ();
            }
            return size;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.DependencyObject che restituisce un figlio del
        ///     tipo selezionato.
        /// </summary>
        ///
        /// <remarks>   Luca, 06/02/2017. </remarks>
        ///
        /// <typeparam name="T">
        ///     Tipo del figlio richiesto.
        /// </typeparam>
        /// <param name="depObj">
        ///     Oggetto padre su cui cercare.
        /// </param>
        ///
        /// <returns>   Il figlio di tipo T. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static T GetChildOfType<T> (this System.Windows.DependencyObject depObj) where T : System.Windows.DependencyObject {
            if (depObj == null) return null;

            for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount (depObj); i++) {
                var child = System.Windows.Media.VisualTreeHelper.GetChild (depObj, i);

                var result = (child as T) ?? GetChildOfType<T> (child);
                if (result != null) return result;
            }
            return null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Ottiene ID imgur da link. </summary>
        ///
        /// <remarks>   Luca, 03/03/2017. </remarks>
        ///
        /// <param name="imgurLink">
        ///     Link a immagine/album imgur.
        /// </param>
        ///
        /// <returns>   ID immagine/album imgur. </returns>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static String ImgurID (String imgurLink) {
            return imgurLink.Substring (imgurLink.LastIndexOf ('/'));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     Un metodo di estensione per System.Windows.DependencyObject che rimuove un figlio.
        /// </summary>
        ///
        /// <remarks>   Luca, 04/03/2017. </remarks>
        ///
        /// <param name="parent">
        ///     Il padre.
        /// </param>
        /// <param name="child">
        ///     Figlio da rimuovere.
        /// </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void RemoveChild (this System.Windows.DependencyObject parent, System.Windows.UIElement child) {
            var panel = parent as Panel;
            if (panel != null) {
                panel.Children.Remove (child);
                return;
            }

            var decorator = parent as System.Windows.Controls.Decorator;
            if (decorator != null) {
                if (decorator.Child == child) {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as System.Windows.Controls.ContentPresenter;
            if (contentPresenter != null) {
                if (contentPresenter.Content == child) {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as System.Windows.Controls.ContentControl;
            if (contentControl != null) {
                if (contentControl.Content == child) {
                    contentControl.Content = null;
                }
                return;
            }

            // maybe more
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.Exceptions {

    internal class ErrorCode {
        public const int NoError = 0x00000000;
        public const int UnknownError = 0x7FFFFFFF;
    }
}
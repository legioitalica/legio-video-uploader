﻿using System;

namespace LegioVideoUploader.Impostazioni {

    internal class Generale {

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private String _tema, _accent;
        private short _logLevel;

        public String Tema {
            get {
                return _tema;
            }
            set {
                _tema = value;
                OnPropertyChanged ("Tema");
            }
        }

        public String Accent {
            get {
                return _accent;
            }
            set {
                _accent = value;
                OnPropertyChanged ("Accent");
            }
        }

        public short LogLevel {
            get {
                return _logLevel;
            }
            set {
                _logLevel = value;
                OnPropertyChanged ("LogLevel");
            }
        }

        public double PesoCache { get; set; }

        public uint MaxDownloadSimultanei { get; set; }

        protected void OnPropertyChanged (string name) {
            System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) {
                handler (this, new System.ComponentModel.PropertyChangedEventArgs (name));
            }
        }
    }
}
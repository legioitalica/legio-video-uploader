﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.Impostazioni {

    internal class WoWData {

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private String _listaIntro, _listaMembri, _listaSfondi, _listaAudio;

        public String ListaIntro {
            get {
                return _listaIntro;
            }
            set {
                _listaIntro = value;
                OnPropertyChanged ("WoW_ListaIntro");
            }
        }

        public String ListaMembri {
            get {
                return _listaMembri;
            }
            set {
                _listaMembri = value;
                OnPropertyChanged ("WoW_ListaMembri");
            }
        }

        public String ListaSfondi {
            get {
                return _listaSfondi;
            }
            set {
                _listaSfondi = value;
                OnPropertyChanged ("WoW_ListaSfondi");
            }
        }

        public String ListaAudio {
            get {
                return _listaAudio;
            }
            set {
                _listaAudio = value;
                OnPropertyChanged ("WoW_ListaAudio");
            }
        }

        public String Region { get; set; }
        public String Locale { get; set; }

        public WoWDataCoda DatiCoda { get; set; }

        protected void OnPropertyChanged (string name) {
            System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) {
                handler (this, new System.ComponentModel.PropertyChangedEventArgs (name));
            }
        }
    }

    internal class WoWDataCoda {
        public String Font { get; set; }
        public double FontSize { get; set; }
        public System.Windows.Media.Brush FontColor { get; set; }
        public System.Windows.Point PosizioneTesto { get; set; }
        public String PatternNome { get; set; }
    }
}
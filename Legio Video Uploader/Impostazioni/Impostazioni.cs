﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.Impostazioni {

    internal class Impostazioni : FX.Configuration.JsonConfiguration {

        public Impostazioni () : base (Utility.getConfigFileName ()) {
        }

        private void Generale_PropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            throw new NotImplementedException ();
        }

        public Generale Generale { get; set; }
        public Data Data { get; set; }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged {
            add {
                Generale.PropertyChanged += value;
                Data.PropertyChanged += value;
            }
            remove {
                Generale.PropertyChanged -= value;
                Data.PropertyChanged -= value;
            }
        }
    }
}
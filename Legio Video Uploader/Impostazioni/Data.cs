﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegioVideoUploader.Impostazioni {
    class Data {
        public WoWData WoW { get; set; }
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged {
            add {
                WoW.PropertyChanged += value;
            }
            remove {
                WoW.PropertyChanged -= value;
            }
        }
    }
}
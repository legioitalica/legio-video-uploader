﻿using System;
using System.Windows.Media;

namespace LegioVideoUploader.UI.Helper {

    internal class FontFamilyHelper {
        public FontFamily Font { get; set; }
        public String Nome { get; set; }

        public FontFamilyHelper (String nome, FontFamily font) {
            Font = font;
            Nome = nome;
        }

        public override bool Equals (Object obj) {
            if (obj.GetType () != typeof (FontFamilyHelper))
                return false;

            FontFamilyHelper other = (FontFamilyHelper) obj;
            return this.Font.Equals (other.Font) && this.Nome.Equals (other.Nome);
        }

        public override int GetHashCode () {
            return base.GetHashCode ();
        }
    }
}
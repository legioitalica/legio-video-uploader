﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace LegioVideoUploader.UI.Componenti {

    /// <summary>
    /// Logica di interazione per ContolloUtenteProgresso.xaml
    /// </summary>
    public partial class ContolloUtenteProgresso : UserControl {
        private System.Windows.Threading.DispatcherTimer timer;
        private TimeSpan trascorso;

        public ContolloUtenteProgresso () {
            InitializeComponent ();
            timer = new System.Windows.Threading.DispatcherTimer ();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan (0, 0, 1);
            trascorso = new TimeSpan (0, 0, 0);
        }

        private void Timer_Tick (object sender, EventArgs e) {
            trascorso = trascorso.Add (new TimeSpan (0, 0, 1));
            TempoTrascorso.Text = trascorso.ToString (@"hh\:mm\:ss");
        }

        public void Start () {
            timer.Start ();
        }

        public void Stop () {
            timer.Stop ();
        }

        private void Progresso_ValueChanged (object sender, RoutedPropertyChangedEventArgs<double> e) {
            String val = e.NewValue.ToString (),
                max = Progresso.Maximum.ToString ();
            if (InBytes) {
                val = Utility.sizeSuffix ((long) e.NewValue, 2);
                max = Utility.sizeSuffix ((long) Progresso.Maximum, 2);
            }

            PercentualeProgresso.Text = String.Format ("{0:0.00}/{1:0.00} ({2:0.00}%)", val, max, Progresso.Maximum != 0 ? (e.NewValue / Progresso.Maximum * 100) : 0);
        }

        public bool InBytes { get; set; }

        public String Titolo {
            get {
                return txtTitolo.Text;
            }
            set {
                txtTitolo.Text = value;
            }
        }

        public double Maximum {
            get {
                return Progresso.Maximum;
            }
            set {
                Progresso.Maximum = value;
            }
        }

        public double Minimum {
            get {
                return Progresso.Minimum;
            }
            set {
                Progresso.Minimum = value;
            }
        }

        public double Value {
            get {
                return Progresso.Value;
            }
            set {
                Progresso.Value = value;
            }
        }

        private void UserControl_Loaded (object sender, RoutedEventArgs e) {
            PercentualeProgresso.Text = String.Format ("{0}/{1} ({2}%)", Progresso.Value, Progresso.Maximum, Progresso.Maximum != 0 ? (Progresso.Value / Progresso.Maximum * 100) : 1000);
            TempoTrascorso.Text = trascorso.ToString (@"hh\:mm\:ss");
        }
    }
}
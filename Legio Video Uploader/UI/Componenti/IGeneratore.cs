﻿using System;
using System.Collections.ObjectModel;

namespace LegioVideoUploader.UI.Componenti {

    internal interface IGeneratore {
        bool PuòElaborare { get; }

        String MotivoErrore { get; }

        void Elabora (String fileName, ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliDownload, ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliProgressoGenerale, ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliElaborazione);

        void ElaboraYoutube (ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliDownload, ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliProgressoGenerale, ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliElaborazione);
    }
}
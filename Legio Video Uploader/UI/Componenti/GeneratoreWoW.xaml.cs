﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using log4net;
using System.Linq;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace LegioVideoUploader.UI.Componenti {

    /// <summary>
    /// Logica di interazione per GeneratoreWoW.xaml
    /// </summary>
    public partial class GeneratoreWoW : UserControl, IGeneratore {
        private Dictionary<String, String> intro, membri, sfondi, audio;
        private KeyValuePair<String, String> introSelezionato, sfondoSelezionato;
        private SortedDictionary<String, String> elencoPartecipanti, partecipanti;
        private OrderedDictionary audioSelezionato;

        private System.Threading.CountdownEvent attesa;

        private ILog logger = LogManager.GetLogger (typeof (GeneratoreWoW));

        public GeneratoreWoW () {
            InitializeComponent ();

            intro = new Dictionary<String, String> ();
            membri = new Dictionary<String, String> ();
            elencoPartecipanti = new SortedDictionary<String, String> ();
            partecipanti = new SortedDictionary<String, String> ();
            sfondi = new Dictionary<String, String> ();
            audio = new Dictionary<String, String> ();
            audioSelezionato = new OrderedDictionary ();

            attesa = new System.Threading.CountdownEvent (1);

            (Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni).PropertyChanged += GeneratoreWoW_PropertyChanged;
        }

        private void GeneratoreWoW_PropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            switch (e.PropertyName) {
                case "WoW_ListaIntro":
                    gestisciIntro ();
                    break;

                case "WoW_ListaMembri":
                    gestisciMembri ();
                    break;

                case "WoW_ListaSfondi":
                    gestisciSfondi ();
                    break;

                case "WoW_ListaAudio":
                    gestisciAudio ();
                    break;

                default:
                    break;
            }
        }

        private void gestisciAudio () {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            String urlAudio = imp.Data.WoW.ListaAudio;

            if (!String.IsNullOrWhiteSpace (urlAudio)) {
                audio.Clear ();
                try {
                    System.Net.WebRequest webRequest = System.Net.WebRequest.Create (urlAudio);
                    using (System.Net.WebResponse response = webRequest.GetResponse ())
                    using (System.IO.Stream content = response.GetResponseStream ())
                    using (System.IO.StreamReader reader = new System.IO.StreamReader (content)) {
                        while (!reader.EndOfStream) {
                            String linea = reader.ReadLine ();
                            Uri uriResult;
                            bool isUri = Uri.TryCreate (linea, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                            if (!isUri) {
                                audio[linea] = reader.ReadLine ();
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.Error ("Errore ottenimento lista audio", e);
                    return;
                }
            }

            logger.Info ("Lista audio ottenuta, creo elenco");
            ListaAudio.Items.Clear ();

            CheckBox cb;
            foreach (KeyValuePair<String, String> data in audio) {
                cb = new CheckBox () {
                    Content = data.Key,
                    DataContext = data.Value
                };
                cb.Checked += listaAudio_Checked;
                cb.Unchecked += listaAudio_Unchecked;
                ListaAudio.Items.Add (cb);
            }
        }

        private void listaAudio_Unchecked (object sender, RoutedEventArgs e) {
            CheckBox cb = (CheckBox) sender;
            String content = (String) cb.Content;
            content = content.Remove (content.LastIndexOf ('(')).Trim ();
            cb.Content = content;
            logger.DebugFormat ("Rimosso sfondo {0} ({1})", cb.Content, cb.DataContext);
            audioSelezionato.Remove (content);
            for (int i = 0; i < audioSelezionato.Count; i++) {
                KeyValuePair<Object, CheckBox> kv = (KeyValuePair<Object, CheckBox>) audioSelezionato[i];
                content = (String) kv.Value.Content;
                content = content.Remove (content.LastIndexOf ('(')).Trim ();
                content += " (" + i + ")";
                kv.Value.Content = content;
            }
        }

        private void listaAudio_Checked (object sender, RoutedEventArgs e) {
            CheckBox cb = (CheckBox) sender;
            logger.DebugFormat ("Aggiunto sfondo {0} ({1})", cb.Content, cb.DataContext);
            KeyValuePair<Object, CheckBox> kv = new KeyValuePair<Object, CheckBox> (cb.DataContext, cb);
            audioSelezionato[cb.Content] = kv;
            cb.Content = String.Format ("{0} ({1})", cb.Content, Utility.indexOfKey (audioSelezionato, cb.Content));
        }

        private void gestisciIntro () {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            String urlIntro = imp.Data.WoW.ListaIntro;

            if (!String.IsNullOrWhiteSpace (urlIntro)) {
                intro.Clear ();
                try {
                    logger.Info ("Ottengo lista intro");
                    System.Net.WebRequest webRequest = System.Net.WebRequest.Create (urlIntro);
                    using (System.Net.WebResponse response = webRequest.GetResponse ())
                    using (System.IO.Stream content = response.GetResponseStream ())
                    using (System.IO.StreamReader reader = new System.IO.StreamReader (content)) {
                        while (!reader.EndOfStream) {
                            String linea = reader.ReadLine ();
                            Uri uriResult;
                            bool isUri = Uri.TryCreate (linea, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                            if (!isUri) {
                                intro[linea] = reader.ReadLine ();
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.Error ("Errore ottenimento lista intro", e);
                    return;
                }
            }

            logger.Info ("Lista intro ottenuta, creo elenco");
            Intro.Items.Clear ();

            RadioButton rb = new RadioButton () {
                Content = "Nessun Intro",
                DataContext = null
            };
            rb.IsChecked = true;
            rb.Checked += intro_Checked;
            Intro.Items.Add (rb);

            foreach (KeyValuePair<String, String> data in intro) {
                rb = new RadioButton () {
                    Content = data.Key,
                    DataContext = data.Value
                };
                rb.Checked += intro_Checked;
                Intro.Items.Add (rb);
            }
        }

        private void gestisciMembri () {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            String urlIntro = imp.Data.WoW.ListaMembri;

            if (!String.IsNullOrWhiteSpace (urlIntro)) {
                membri.Clear ();
                try {
                    logger.Info ("Ottengo lista membri");
                    System.Net.WebRequest webRequest = System.Net.WebRequest.Create (urlIntro);
                    using (System.Net.WebResponse response = webRequest.GetResponse ())
                    using (System.IO.Stream content = response.GetResponseStream ())
                    using (System.IO.StreamReader reader = new System.IO.StreamReader (content)) {
                        while (!reader.EndOfStream) {
                            String linea = reader.ReadLine ();
                            Uri uriResult;
                            bool isUri = Uri.TryCreate (linea, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                            if (!isUri) {
                                membri[linea] = reader.ReadLine ();
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.Error ("Errore ottenimento lista membri", e);
                    return;
                }
            }

            logger.Info ("Lista membri ottenuta, creo elenco");
            ListaMembri.Items.Clear ();

            RadioButton rb;
            foreach (KeyValuePair<String, String> data in membri) {
                rb = new RadioButton () {
                    Content = data.Key,
                    DataContext = data.Value
                };
                rb.Checked += listaMembri_Checked;
                ListaMembri.Items.Add (rb);
            }
        }

        private async void gestisciSfondi () {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            String urlIntro = imp.Data.WoW.ListaSfondi;

            if (!String.IsNullOrWhiteSpace (urlIntro)) {
                sfondi.Clear ();
                try {
                    logger.Info ("Ottengo lista sfondi");
                    var client = new Imgur.API.Authentication.Impl.ImgurClient (ApiKeys.ImgurClientID, ApiKeys.ImgurClientSecret);
                    var endpoint = new Imgur.API.Endpoints.Impl.AlbumEndpoint (client);
                    var album = await endpoint.GetAlbumImagesAsync (imp.Data.WoW.ListaSfondi);
                    foreach (Imgur.API.Models.IImage img in album) {
                        sfondi[img.Title] = img.Link;
                    }
                } catch (Exception e) {
                    logger.Error ("Errore ottenimento lista sfondi", e);
                    return;
                }
            }

            logger.Info ("Lista sfondi ottenuta, creo elenco");
            ListaSfondi.Items.Clear ();

            RadioButton cb;
            foreach (KeyValuePair<String, String> data in sfondi) {
                cb = new RadioButton () {
                    Content = data.Key,
                    DataContext = data.Value
                };
                cb.Checked += listaSfondi_Checked;
                Image img = new Image ();
                img.Source = new BitmapImage (new Uri (data.Value));
                cb.ToolTip = img;
                ListaSfondi.Items.Add (cb);
            }
        }

        private void listaSfondi_Checked (object sender, RoutedEventArgs e) {
            RadioButton cb = (RadioButton) sender;
            logger.DebugFormat ("Selezionato sfondo {0} ({1})", cb.Content, cb.DataContext);
            sfondoSelezionato = new KeyValuePair<string, string> ((String) cb.Content, (String) cb.DataContext);
        }

        private void listaMembri_Checked (object sender, RoutedEventArgs e) {
            RadioButton rb = (RadioButton) sender;
            logger.DebugFormat ("Selezionata lista {0} ({1})", rb.Content, rb.DataContext);
            String urlPlaylist = (String) (rb).DataContext;

            CheckBox cb;
            ElencoPartecipanti.Items.Clear ();
            Uri playlistUri = new Uri (urlPlaylist);
            var query = System.Web.HttpUtility.ParseQueryString (playlistUri.Query);

            //Ottengo elenco video da playlist
            System.Threading.ThreadPool.QueueUserWorkItem (new System.Threading.WaitCallback (delegate {
                Google.Apis.YouTube.v3.YouTubeService YouTubeService = new Google.Apis.YouTube.v3.YouTubeService (new Google.Apis.Services.BaseClientService.Initializer () { ApiKey = ApiKeys.YouTubeApiKey });
                String PageToken = "";
                while (PageToken != null) {
                    var PlaylistRequest = YouTubeService.PlaylistItems.List ("snippet");

                    PlaylistRequest.PlaylistId = query["list"];
                    PlaylistRequest.MaxResults = 50;
                    PlaylistRequest.PageToken = PageToken;

                    var PlaylistResponse = PlaylistRequest.Execute ();

                    foreach (var video in PlaylistResponse.Items) {
                        elencoPartecipanti[video.Snippet.Title] = String.Format ("https://www.youtube.com/watch?v={0}", video.Snippet.ResourceId.VideoId);
                    }

                    PageToken = PlaylistResponse.NextPageToken;
                }

                foreach (KeyValuePair<String, String> data in elencoPartecipanti) {
                    ElencoPartecipanti.Dispatcher.Invoke (new Action (() => {
                        cb = new CheckBox () { Content = data.Key, DataContext = data.Value };
                        cb.Checked += partecipanti_Checked;
                        cb.Unchecked += partecipanti_Unchecked;
                        ElencoPartecipanti.Items.Add (cb);
                    }));
                    System.Threading.Thread.Sleep (100);
                }
            }));
        }

        private void intro_Checked (object sender, RoutedEventArgs e) {
            RadioButton rb = (RadioButton) sender;
            logger.DebugFormat ("Selezionato intro {0} ({1})", rb.Content, rb.DataContext);
            introSelezionato = new KeyValuePair<string, string> ((String) rb.Content, (String) rb.DataContext);
        }

        private void UserControl_Loaded (object sender, RoutedEventArgs e) {
            gestisciIntro ();
            gestisciMembri ();
            gestisciSfondi ();
            gestisciAudio ();
        }

        private void partecipanti_Unchecked (object sender, RoutedEventArgs e) {
            partecipanti.Remove ((String) ((CheckBox) sender).Content);
        }

        private void partecipanti_Checked (object sender, RoutedEventArgs e) {
            CheckBox cb = (CheckBox) sender;
            partecipanti[(String) cb.Content] = (String) cb.DataContext;
        }

        public void Elabora (string fileName, ObservableCollection<ContolloUtenteProgresso> controlliDownload, ObservableCollection<ContolloUtenteProgresso> controlliProgressoGenerale, ObservableCollection<ContolloUtenteProgresso> controlliElaborazione) {
            App.Current.Dispatcher.Invoke (new Action (() => {
                controlliDownload.Clear ();
                controlliElaborazione.Clear ();
                controlliProgressoGenerale.Clear ();
            }));

            Finestre.MainWindow main = null;
            App.Current.Dispatcher.Invoke (new Action (() => { main = (Finestre.MainWindow) App.Current.MainWindow; }));
            attesa = new System.Threading.CountdownEvent (1);
            logger.Info ("Inizio elaborazione");
            logger.Info ("Controllo file partecipanti, in caso non presenti scarico");
            VideoManager.DownloadManager.Istance.DownloadFinito += Istance_DownloadFinito;
            List<Database.CacheData> videoPartecipanti = new List<Database.CacheData> ();
            List<Database.CacheData> audioSfondo = new List<Database.CacheData> ();

            Componenti.ContolloUtenteProgresso cup;

            SQLite.SQLiteConnection conn = (SQLite.SQLiteConnection) App.Current.Properties["Database"];

            Database.CacheData sfondo = VideoManager.CacheManager.Istance.getImage (sfondoSelezionato.Key, sfondoSelezionato.Value);
            sfondo.InUso = true;
            sfondo.NumeroUtilizzi++;
            conn.Update (sfondo);
            if (!sfondo.Scaricato) {
                attesa.AddCount ();
                VideoManager.DownloadManager.Istance.Download (sfondo, out cup);

                cup.Dispatcher.Invoke (new Action (() => { cup.Titolo = String.Format ("Download di {0} ({1})", sfondo.Nome, sfondo.Url); }));

                controlliProgressoGenerale.Add (cup);
                controlliDownload.Add (cup);

                main.ProgressoDownload.Dispatcher.Invoke (new Action (() => {
                    main.ProgressoDownload.Maximum++;
                }));
            }

            String[] keys = new String[audioSelezionato.Count];
            audioSelezionato.Keys.CopyTo (keys, 0);
            for (int i = 0; i < keys.Length; i++) {
                KeyValuePair<Object, CheckBox> kv = (KeyValuePair<Object, CheckBox>) audioSelezionato[i];
                String url = "";
                kv.Value.Dispatcher.Invoke (new Action (() => {
                    url = (String) kv.Value.DataContext;
                }));
                Database.CacheData video = VideoManager.CacheManager.Istance.getAudio ((String) keys[i], url);
                video.InUso = true;
                video.NumeroUtilizzi++;
                conn.Update (video);
                audioSfondo.Add (video);
                if (!video.Scaricato) {
                    attesa.AddCount ();
                    VideoManager.DownloadManager.Istance.Download (video, out cup);
                    cup.Dispatcher.Invoke (new Action (() => { cup.Titolo = String.Format ("Download di {0} ({1})", video.Nome, video.Url); }));

                    main.ProgressoDownload.Dispatcher.Invoke (new Action (() => {
                        controlliProgressoGenerale.Add (cup);
                        controlliDownload.Add (cup);
                        main.ProgressoDownload.Maximum++;
                    }));
                }
            }

            foreach (KeyValuePair<String, String> kv in partecipanti) {
                Database.CacheData video = VideoManager.CacheManager.Istance.getVideo (kv.Key, kv.Value);
                video.InUso = true;
                video.NumeroUtilizzi++;
                conn.Update (video);
                videoPartecipanti.Add (video);
                if (!video.Scaricato) {
                    attesa.AddCount ();
                    VideoManager.DownloadManager.Istance.Download (video, out cup);
                    cup.Dispatcher.Invoke (new Action (() => { cup.Titolo = String.Format ("Download di {0} ({1})", video.Nome, video.Url); }));

                    main.ProgressoDownload.Dispatcher.Invoke (new Action (() => {
                        controlliProgressoGenerale.Add (cup);
                        controlliDownload.Add (cup);
                        main.ProgressoDownload.Maximum++;
                    }));
                }
            }

            attesa.Signal ();
            //Vari FFMPEG
            attesa.Wait ();
            logger.Info ("Tutti i download sono stati completati, procedo all'elaborazione");
            main.ProgressoDownload.Stop ();
            VideoManager.DownloadManager.Istance.DownloadFinito -= Istance_DownloadFinito;
            main.ProgressoGenerale.Dispatcher.Invoke (new Action (() => {
                main.ProgressoGenerale.Value++;
            }));

            main.ProgressoElaborazione.Dispatcher.Invoke (new Action (() => {
                main.ProgressoElaborazione.Maximum = 3 + videoPartecipanti.Count;
            }));
            main.ProgressoElaborazione.Start ();

            TimeSpan durataMembri = new TimeSpan (0);
            var ffProbe = new NReco.VideoInfo.FFProbe ();
            ffProbe.ToolPath = System.IO.Path.GetTempPath ();

            foreach (Database.CacheData video in videoPartecipanti) {
                var videoInfo = ffProbe.GetMediaInfo (video.FileName);
                durataMembri = durataMembri.Add (videoInfo.Duration);
            }

            TimeSpan durataParzialeSfondo = new TimeSpan (0);
            List<String> sfondiDaUsare = new List<String> ();
            int index = 0;
            while (durataParzialeSfondo < durataMembri) {
                var videoInfo = ffProbe.GetMediaInfo (audioSfondo[index].FileName);
                sfondiDaUsare.Add (audioSfondo[index].FileName);
                index = (index + 1) % audioSfondo.Count;
                durataParzialeSfondo = durataParzialeSfondo.Add (videoInfo.Duration);
            }

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter ();
            ffMpeg.LogReceived += FfMpeg_LogReceived;

            ffMpeg.FFMpegToolPath = System.IO.Path.GetTempPath ();
            int count = 0;
            String codaFileName = sfondiDaUsare[0];

            EventHandler<NReco.VideoConverter.ConvertProgressEventArgs> handler;
            String newCoda;
            //Genero coda temporanea concatenando tutti i video di sfondo
            if (sfondiDaUsare.Count > 1) {
                logger.Info ("Generazione audio di sfondo");
                cup = App.Current.Dispatcher.Invoke (delegate {
                    UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                    tmp.Titolo = "Preparazione Audio Sfondo";
                    return tmp;
                });

                handler = (sender, e) => {
                    FfMpeg_ConvertProgress (sender, e, cup);
                };

                ffMpeg.ConvertProgress += handler;

                main.Dispatcher.Invoke (new Action (() => {
                    controlliElaborazione.Add (cup);
                    controlliProgressoGenerale.Add (cup);
                }));

                cup.Start ();
                NReco.VideoConverter.ConcatSettings set = new NReco.VideoConverter.ConcatSettings () {
                    MaxDuration = (float) durataMembri.TotalSeconds,
                    CustomOutputArgs = String.Format ("-af \"afade=t=out:st={0}:d=4:curve=log\"", (durataMembri.TotalSeconds - 4).ToString ("F", System.Globalization.CultureInfo.InvariantCulture))
                };
                codaFileName = System.IO.Path.Combine (Utility.getTempFolder (), "audio.flac");
                ffMpeg.ConcatMedia (sfondiDaUsare.ToArray (), codaFileName, NReco.VideoConverter.Format.flac, set);
                ffMpeg.ConvertProgress -= handler;

                cup.Stop ();

                main.ProgressoElaborazione.Dispatcher.Invoke (new Action (() => { main.ProgressoElaborazione.Value++; }));
            } else {
                //Taglio lo sfondo in base alla durata
                cup = App.Current.Dispatcher.Invoke (delegate {
                    UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                    tmp.Titolo = "Preparazione Audio";
                    return tmp;
                });

                main.Dispatcher.Invoke (new Action (() => {
                    controlliElaborazione.Add (cup);
                    controlliProgressoGenerale.Add (cup);
                }));

                handler = (sender, e) => {
                    FfMpeg_ConvertProgress (sender, e, cup);
                };
                ffMpeg.ConvertProgress += handler;
                newCoda = System.IO.Path.Combine (Utility.getTempFolder (), "audio.flac");

                NReco.VideoConverter.ConvertSettings settings = new NReco.VideoConverter.ConvertSettings () {
                    MaxDuration = (float) durataMembri.TotalSeconds,
                    CustomOutputArgs = String.Format ("-af \"afade=t=out:st={0}:d=4:curve=log\"", (durataMembri.TotalSeconds - 4).ToString ("F", System.Globalization.CultureInfo.InvariantCulture))
                };
                cup.Start ();
                ffMpeg.ConvertMedia (codaFileName, null, newCoda, NReco.VideoConverter.Format.flac, settings);
                ffMpeg.ConvertProgress -= handler;
                cup.Stop ();

                main.ProgressoElaborazione.Dispatcher.Invoke (new Action (() => { main.ProgressoElaborazione.Value++; }));

                codaFileName = newCoda;
            }

            Impostazioni.Impostazioni imp = (Impostazioni.Impostazioni) App.Current.Properties["Impostazioni"];

            String patternNome = imp.Data.WoW.DatiCoda.PatternNome;
            patternNome = System.Text.RegularExpressions.Regex.Escape (patternNome);

            int gruppoNome = 0, gruppoServer = 0;
            if (patternNome.IndexOf ("%player") < patternNome.IndexOf ("%server")) {
                gruppoServer = 2;
                gruppoNome = 1;
            } else {
                gruppoServer = 1;
                gruppoNome = 2;
            }

            patternNome = patternNome.Replace ("%player", @"(\p{L}+)").Replace ("%server", @"([\p{L} \p{P}]*)").Replace ("%exp", @"[\p{L} \p{P}]*").Replace ("%text", @"[\p{L} \p{P}]*");

            WowDotNetAPI.Region reg = (WowDotNetAPI.Region) Enum.Parse (typeof (WowDotNetAPI.Region), imp.Data.WoW.Region);
            WowDotNetAPI.Locale loc = (WowDotNetAPI.Locale) Enum.Parse (typeof (WowDotNetAPI.Locale), imp.Data.WoW.Locale);
            WowDotNetAPI.WowExplorer expl = new WowDotNetAPI.WowExplorer (reg, loc, ApiKeys.BattleNetKey);
            var classInfo = expl.GetCharacterClasses ();

            String opzioniFiltro;
            NReco.VideoConverter.ConvertSettings outImp;
            String pathBackground = System.IO.Path.Combine (Utility.getTempFolder (), "background.png");

            //Genero sfondi partecipanti
            foreach (Database.CacheData video in videoPartecipanti) {
                var matches = System.Text.RegularExpressions.Regex.Match (video.Nome, patternNome);
                if (matches.Success) {
                    String nome = matches.Groups[gruppoNome].Value;
                    String server = matches.Groups[gruppoServer].Value;

                    logger.InfoFormat ("Ottengo dati player {0} - {1}", nome, server);
                    var player = expl.GetCharacter (server, nome, WowDotNetAPI.CharacterOptions.GetEverything);
                    String playerTXT = String.Format ("{0}\n<{1}>\n{2} {3} {4}",
                        player.Name,
                        player.Guild.Name,
                        player.Level,
                        System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase (player.Race.ToString ().Replace ('_', ' ').ToLower ()),
                        System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase (player.Class.ToString ().Replace ('_', ' ').ToLower ()));

                    App.Current.Dispatcher.Invoke (new Action (() => {
                        var visual = new DrawingVisual ();
                        var img = new BitmapImage (new Uri (sfondo.FileName));

                        using (DrawingContext drawingContext = visual.RenderOpen ()) {
                            drawingContext.DrawImage (img, new Rect (new Size (img.Width, img.Height)));
                            FormattedText txt = new FormattedText (playerTXT, System.Globalization.CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                                    new Typeface (imp.Data.WoW.DatiCoda.Font), imp.Data.WoW.DatiCoda.FontSize, imp.Data.WoW.DatiCoda.FontColor);
                            txt.TextAlignment = TextAlignment.Center;
                            txt.SetFontWeight (FontWeights.Bold);
                            Point pos = new Point ();
                            pos.X = imp.Data.WoW.DatiCoda.PosizioneTesto.X + txt.WidthIncludingTrailingWhitespace / 2;
                            pos.Y = imp.Data.WoW.DatiCoda.PosizioneTesto.Y;
                            drawingContext.DrawText (txt, pos);
                        }

                        RenderTargetBitmap rtb = new RenderTargetBitmap ((int) img.Width, (int) img.Height, 96, 96, System.Windows.Media.PixelFormats.Default);
                        rtb.Render (visual);

                        PngBitmapEncoder encoder = new PngBitmapEncoder ();
                        encoder.Frames.Add (BitmapFrame.Create (rtb));
                        using (System.IO.Stream str = System.IO.File.Open (pathBackground, System.IO.FileMode.OpenOrCreate)) {
                            encoder.Save (str);
                        }
                    }));

                    logger.InfoFormat ("Genero Video Coda per {0} - {1}", nome, server);

                    newCoda = System.IO.Path.Combine (Utility.getTempFolder (), "coda_" + count++ + ".mp4");
                    cup = App.Current.Dispatcher.Invoke (delegate {
                        UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                        tmp.Titolo = "Preparazione Video Coda per " + nome + " - " + server;
                        return tmp;
                    });

                    main.Dispatcher.Invoke (new Action (() => {
                        controlliElaborazione.Add (cup);
                        controlliProgressoGenerale.Add (cup);
                    }));

                    handler = (sender, e) => {
                        FfMpeg_ConvertProgress (sender, e, cup);
                    };
                    ffMpeg.ConvertProgress += handler;

                    opzioniFiltro = "-filter_complex \"[0:v] scale = 1920x1080[base];[1:v] colorkey=0x00FF00:0.1:0.15, scale=1920x1080 [tmp];[base][tmp]overlay=main_w-overlay_w-10:main_h-overlay_h-10\"";
                    outImp = new NReco.VideoConverter.ConvertSettings () {
                        CustomOutputArgs = opzioniFiltro
                    };

                    NReco.VideoConverter.FFMpegInput in1 = new NReco.VideoConverter.FFMpegInput (pathBackground);
                    NReco.VideoConverter.FFMpegInput in2 = new NReco.VideoConverter.FFMpegInput (video.FileName);

                    cup.Start ();
                    ffMpeg.ConvertMedia (new NReco.VideoConverter.FFMpegInput[] { in1, in2 }, newCoda, NReco.VideoConverter.Format.mp4, outImp);
                    ffMpeg.ConvertProgress -= handler;
                    cup.Stop ();

                    main.ProgressoElaborazione.Dispatcher.Invoke (new Action (() => { main.ProgressoElaborazione.Value++; }));

                    logger.InfoFormat ("Generazione Video Coda per {0} - {1} completato", nome, server);
                }
            }
            //Genero Coda
            logger.Info ("Genero Coda");

            String[] membri = new string[count];
            for (int i = 0; i < count; i++)
                membri[i] = System.IO.Path.Combine (Utility.getTempFolder (), "coda_" + i + ".mp4");

            newCoda = System.IO.Path.Combine (Utility.getTempFolder (), "coda_" + count + ".mp4");
            cup = App.Current.Dispatcher.Invoke (delegate {
                UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                tmp.Titolo = "Preparazione Coda";
                return tmp;
            });

            main.Dispatcher.Invoke (new Action (() => {
                controlliElaborazione.Add (cup);
                controlliProgressoGenerale.Add (cup);
            }));

            handler = (sender, e) => {
                FfMpeg_ConvertProgress (sender, e, cup);
            };
            ffMpeg.ConvertProgress += handler;

            cup.Start ();
            ffMpeg.ConcatMedia (membri, newCoda, NReco.VideoConverter.Format.mp4, new NReco.VideoConverter.ConcatSettings ());
            ffMpeg.ConvertProgress -= handler;
            cup.Stop ();

            logger.Info ("Unione Coda e Audio");
            NReco.VideoConverter.FFMpegInput i1 = new NReco.VideoConverter.FFMpegInput (newCoda);
            NReco.VideoConverter.FFMpegInput i2 = new NReco.VideoConverter.FFMpegInput (System.IO.Path.Combine (Utility.getTempFolder (), "audio.flac"));

            newCoda = System.IO.Path.Combine (Utility.getTempFolder (), "coda_" + ++count + ".mp4");
            cup = App.Current.Dispatcher.Invoke (delegate {
                UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
                tmp.Titolo = "Unione Coda e Audio";
                return tmp;
            });

            main.Dispatcher.Invoke (new Action (() => {
                controlliElaborazione.Add (cup);
                controlliProgressoGenerale.Add (cup);
            }));

            handler = (sender, e) => {
                FfMpeg_ConvertProgress (sender, e, cup);
            };
            ffMpeg.ConvertProgress += handler;
            NReco.VideoConverter.ConvertSettings sett = new NReco.VideoConverter.ConvertSettings () {
                CustomOutputArgs = "-map 0:v -map 1:a -c:v libx264 -c:a aac"
            };

            cup.Start ();
            ffMpeg.ConvertMedia (new NReco.VideoConverter.FFMpegInput[] { i1, i2 }, newCoda, NReco.VideoConverter.Format.mp4, sett);

            ffMpeg.ConvertProgress -= handler;
            cup.Stop ();
            logger.Info ("Generazione Coda Completata");
            //count++;
            //newCoda = System.IO.Path.Combine (Utility.getTempFolder (), "coda_" + count + ".mp4");
            //cup = App.Current.Dispatcher.Invoke (delegate {
            //    UI.Componenti.ContolloUtenteProgresso tmp = new UI.Componenti.ContolloUtenteProgresso ();
            //    tmp.Titolo = "Preparazione Coda";
            //    return tmp;
            //});
            //controlliElaborazione.Add (cup);
            //handler = (sender, e) => {
            //    FfMpeg_ConvertProgress (sender, e, cup);
            //};
            //ffMpeg.ConvertProgress += handler;

            //opzioniFiltro = "-filter_complex \"[0:v] scale = 1920x1080[base];[1:v] colorkey=0x00FF00:0.1:0.15, scale=1920x1080 [tmp];[base][tmp]overlay=main_w-overlay_w-10:main_h-overlay_h-10\"";
            //outImp = new NReco.VideoConverter.ConvertSettings () {
            //    CustomOutputArgs = opzioniFiltro
            //};

            //NReco.VideoConverter.FFMpegInput i1 = new NReco.VideoConverter.FFMpegInput (codaFileName);
            //NReco.VideoConverter.FFMpegInput i2 = new NReco.VideoConverter.FFMpegInput (videoPartecipanti[0].FileName);

            //cup.Start ();
            //ffMpeg.ConvertMedia (new NReco.VideoConverter.FFMpegInput[] { i1, i2 }, newCoda, NReco.VideoConverter.Format.mp4, outImp);
            //ffMpeg.ConvertProgress -= handler;
            //cup.Stop ();
            //logger.Info ("Generazione Coda Completata");

            //Terminata Elaborazione
            main.ProgressoGenerale.Dispatcher.Invoke (new Action (() => {
                main.ProgressoGenerale.Value++;
                main.ProgressoElaborazione.Value++;
            }));

            main.ProgressoGenerale.Stop ();
            main.ProgressoElaborazione.Stop ();

            //"Sblocco" i video utilizzati
            foreach (Database.CacheData video in audioSfondo) {
                video.InUso = false;
                conn.Update (video);
            }
            foreach (Database.CacheData video in videoPartecipanti) {
                video.InUso = false;
                conn.Update (video);
            }

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo (Utility.getTempFolder ());

            foreach (System.IO.FileInfo file in di.GetFiles ()) {
                file.Delete ();
            }
            foreach (System.IO.DirectoryInfo dir in di.GetDirectories ()) {
                dir.Delete (true);
            }

            main.Elabora.Dispatcher.Invoke (new Action (() => { main.Elabora.IsEnabled = true; }));

            main.ElaboraInvia.Dispatcher.Invoke (new Action (() => { main.ElaboraInvia.IsEnabled = true; }));
        }

        private void FfMpeg_LogReceived (object sender, NReco.VideoConverter.FFMpegLogEventArgs e) {
            logger.Debug ("ffmpeg: " + e.Data);
        }

        public void ElaboraYoutube (ObservableCollection<ContolloUtenteProgresso> controlliDownload, ObservableCollection<ContolloUtenteProgresso> controlliProgressoGenerale, ObservableCollection<ContolloUtenteProgresso> controlliElaborazione) {
            throw new NotImplementedException ();
        }

        public bool PuòElaborare {
            get {
                return !String.IsNullOrEmpty (sfondoSelezionato.Value) && partecipanti.Count > 0 && audioSelezionato.Count > 0;
            }
        }

        public String MotivoErrore {
            get {
                String err = "";

                if (String.IsNullOrEmpty (sfondoSelezionato.Value))
                    err += "Bisogna selezionare uno sfondo\n";

                if (partecipanti.Count == 0)
                    err += "Bisogna selezionare almeno un partecipante\n";

                if (audioSelezionato.Count == 0)
                    err += "Bisogna selezionare almeno una traccia audio";

                return err;
            }
        }

        private void FfMpeg_ConvertProgress (object sender, NReco.VideoConverter.ConvertProgressEventArgs e, Componenti.ContolloUtenteProgresso cup) {
            cup.Dispatcher.Invoke (new Action (() => {
                cup.Maximum = e.TotalDuration.Ticks;
                cup.Value = e.Processed.Ticks;
            }));
        }

        private void Istance_DownloadFinito (object sender, VideoManager.DownloadEventArgs e) {
            attesa.Signal ();
            App.Current.Dispatcher.Invoke (new Action (() => { ((Finestre.MainWindow) App.Current.MainWindow).ProgressoDownload.Value++; }));
        }
    }
}
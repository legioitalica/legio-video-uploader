﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using log4net;
using MahApps.Metro.SimpleChildWindow;
using MahApps.Metro.Controls.Dialogs;
using System.Collections.ObjectModel;

namespace LegioVideoUploader.UI.Finestre {

    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MahApps.Metro.Controls.MetroWindow {
        private MahApps.Metro.SimpleChildWindow.ChildWindow child;
        private Componenti.IGeneratore generatore;

        private ObservableCollection<UI.Componenti.ContolloUtenteProgresso> controlliDownload, controlliProgressoGenerale, controlliElaborazione;

        private ILog logger = LogManager.GetLogger (typeof (MainWindow));

        public MainWindow () {
            InitializeComponent ();

            controlliDownload = new ObservableCollection<Componenti.ContolloUtenteProgresso> ();
            controlliProgressoGenerale = new ObservableCollection<Componenti.ContolloUtenteProgresso> ();
            controlliElaborazione = new ObservableCollection<Componenti.ContolloUtenteProgresso> ();

            generatore = Test;
        }

        private void Bitbucket_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start ("https://bitbucket.org/legioitalica/legio-video-uploader");
        }

        private async void Impostazioni_Click (object sender, RoutedEventArgs e) {
            if (child != null && child.IsOpen)
                return;
            child = new Impostazioni () { IsModal = true, AllowMove = false };
            await this.ShowChildWindowAsync (child);
        }

        private async void About_Click (object sender, RoutedEventArgs e) {
            if (child != null && child.IsOpen)
                return;
            child = new InformazioniSu () { IsModal = true, AllowMove = false };
            await this.ShowChildWindowAsync (child);
        }

        private void PayPal_Click (object sender, RoutedEventArgs e) {
            System.Diagnostics.Process.Start (ApiKeys.PayPalLink);
        }

        private void MetroWindow_Loaded (object sender, RoutedEventArgs e) {
        }

        private async void Elabora_Click (object sender, RoutedEventArgs e) {
            ProgressoGenerale.Maximum = 2;
            if (!generatore.PuòElaborare) {
                await this.ShowMessageAsync ("Errore", generatore.MotivoErrore);
                return;
            }
            Microsoft.Win32.SaveFileDialog dial = new Microsoft.Win32.SaveFileDialog ();
            dial.FileName = "Video";
            dial.DefaultExt = ".mp4";
            dial.Filter = "MP4 video file (.mp4)|*.mp4";
            Nullable<bool> result = dial.ShowDialog ();
            if (result == true) {
                Elabora.IsEnabled = false;
                ElaboraInvia.IsEnabled = false;
                System.Threading.ThreadPool.QueueUserWorkItem (new System.Threading.WaitCallback (delegate (object state) {
                    Test.Elabora (dial.FileName, controlliDownload, controlliProgressoGenerale, controlliElaborazione);
                }));
            }
        }

        private void ProgressoGenerale_MouseDown (object sender, System.Windows.Input.MouseButtonEventArgs e) {
            FlyoutProgresso.IsOpen = true;
        }

        private void ProgressoDownload_MouseDown (object sender, System.Windows.Input.MouseButtonEventArgs e) {
            FlyoutDownload.IsOpen = true;
        }

        private void FlyoutProgresso_IsVisibleChanged (object sender, DependencyPropertyChangedEventArgs e) {
            if (FlyoutProgresso.IsVisible == true) {
                DockFlyoutProgresso.ItemsSource = controlliProgressoGenerale;
                //    FlyoutProgresso.Dispatcher.Invoke (new Action (() => {
                //        foreach (UI.Componenti.ContolloUtenteProgresso controllo in controlliProgressoGenerale) {
                //            if (controllo.Parent != null)
                //                controllo.Parent.RemoveChild (controllo);
                //            DockFlyoutProgresso.Children.Add (controllo);
                //            DockPanel.SetDock (controllo, Dock.Top);
                //        }
                //        Label lbl = new Label ();
                //        DockFlyoutProgresso.Children.Add (lbl);
                //        DockPanel.SetDock (lbl, Dock.Top);
                //    }));
            } else
                DockFlyoutProgresso.ItemsSource = null;
        }

        private void FlyoutDownload_IsVisibleChanged (object sender, DependencyPropertyChangedEventArgs e) {
            if (FlyoutDownload.IsVisible == true)
                DockFlyoutDownload.ItemsSource = controlliDownload;
            else
                DockFlyoutDownload.ItemsSource = null;
        }

        private void FlyoutElaborazione_IsVisibleChanged (object sender, DependencyPropertyChangedEventArgs e) {
            if (FlyoutDownload.IsVisible == true)
                DockFlyoutElaborazione.ItemsSource = controlliElaborazione;
            else
                DockFlyoutElaborazione.ItemsSource = null;
        }

        private void ProgressoElaborazione_MouseDown (object sender, System.Windows.Input.MouseButtonEventArgs e) {
            FlyoutElaborazione.IsOpen = true;
        }

        private void elaboraThread (String fileName) {
            ProgressoGenerale.Start ();
        }

        private void uploadThread () {
        }
    }
}
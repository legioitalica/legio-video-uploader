﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LegioVideoUploader.UI.Finestre {

    /// <summary>
    /// Logica di interazione per InformazioniSu.xaml
    /// </summary>
    public partial class InformazioniSu : MahApps.Metro.SimpleChildWindow.ChildWindow {

        public InformazioniSu () {
            InitializeComponent ();
        }

        private void Window_Loaded (object sender, RoutedEventArgs e) {
            Versione.Text = "Versione Corrente: " + System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Version;
            Copyright.Text = System.Diagnostics.FileVersionInfo.GetVersionInfo (System.Reflection.Assembly.GetEntryAssembly ().Location).LegalCopyright;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using log4net;
using MoreLinq;

namespace LegioVideoUploader.UI.Finestre {

    /// <summary>
    /// Logica di interazione per Impostazioni.xaml
    /// </summary>
    public partial class Impostazioni : MahApps.Metro.SimpleChildWindow.ChildWindow {
        private ILog logger = LogManager.GetLogger (typeof (Impostazioni));
        private bool caricato = false;
        private ICollection<Helper.FontFamilyHelper> _fonts;
        private bool isMoving;
        private UIElement movingControl;
        private Point relativePosition;

        private readonly double[] _defaultFontSize = {
              3.0,    4.0,   5.0,   6.0,   6.5,   7.0,   7.5,   8.0,   8.5,   9.0,
              9.5,   10.0,  10.5,  11.0,  11.5,  12.0,  12.5,  13.0,  13.5,  14.0,
             15.0,   16.0,  17.0,  18.0,  19.0,  20.0,  22.0,  24.0,  26.0,  28.0,
             30.0,   32.0,  34.0,  36.0,  38.0,  40.0,  44.0,  48.0,  52.0,  56.0,
             60.0,   64.0,  68.0,  72.0,  76.0,  80.0,  88.0,  96.0, 100.0, 104.0,
            112.0,  120.0, 128.0, 136.0, 144.0
        };

        public Impostazioni () {
            InitializeComponent ();

            _fonts = new List<Helper.FontFamilyHelper> ();
            System.Windows.Markup.XmlLanguage current = System.Windows.Markup.XmlLanguage.GetLanguage (System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag);

            ICollection<FontFamily> tempList = Fonts.GetFontFamilies (new Uri ("pack://application:,,,/"), "./Resources/Fonts/").Union (Fonts.SystemFontFamilies).ToList ();
            foreach (FontFamily font in tempList) {
                String fontName;
                if (font.FamilyNames.ContainsKey (current))
                    fontName = font.FamilyNames[current];
                else
                    fontName = font.FamilyNames.First ().Value;

                _fonts.Add (new Helper.FontFamilyHelper (fontName, font));
            }
            _fonts = _fonts.OrderBy (x => x.Nome).DistinctBy (x => x.Nome).ToList ();
            Font.ItemsSource = _fonts;
            FontSize.ItemsSource = _defaultFontSize;
            isMoving = false;
        }

        private void ChildWindow_Loaded (object sender, RoutedEventArgs e) {
            WoW_Region.ItemsSource = Enum.GetValues (typeof (WowDotNetAPI.Region)).Cast<WowDotNetAPI.Region> ();
            WoW_Locale.ItemsSource = Enum.GetValues (typeof (WowDotNetAPI.Locale)).Cast<WowDotNetAPI.Locale> ();

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;

            if (!String.IsNullOrEmpty (imp.Data.WoW.ListaIntro))
                ListaIntro.Text = imp.Data.WoW.ListaIntro;

            if (!String.IsNullOrEmpty (imp.Data.WoW.ListaMembri))
                ListaMembri.Text = imp.Data.WoW.ListaMembri;

            if (!String.IsNullOrEmpty (imp.Data.WoW.ListaSfondi))
                ListaSfondo.Text = imp.Data.WoW.ListaSfondi;

            if (!String.IsNullOrEmpty (imp.Data.WoW.ListaAudio))
                ListaAudio.Text = imp.Data.WoW.ListaAudio;

            LivelloLog.SelectedIndex = imp.Generale.LogLevel;

            double peso = imp.Generale.PesoCache;
            int index = (int) Math.Log (peso, 1024);
            peso = peso / (1L << (index * 10));

            PesoCacheCombo.SelectedIndex = index;
            PesoCacheUpDown.Value = peso;

            MaxDownload.Value = imp.Generale.MaxDownloadSimultanei;

            int selectedIndex = 0;

            selectedIndex = _fonts.ToList ().FindIndex (x => x.Nome == imp.Data.WoW.DatiCoda.Font);

            Font.SelectedIndex = selectedIndex;
            selectedIndex = 0;
            if (_defaultFontSize.Contains (imp.Data.WoW.DatiCoda.FontSize))
                selectedIndex = _defaultFontSize.ToList ().IndexOf (imp.Data.WoW.DatiCoda.FontSize);

            FontSize.SelectedIndex = selectedIndex;

            if (imp.Data.WoW.DatiCoda.FontColor != null && imp.Data.WoW.DatiCoda.FontColor.GetType ().Equals (typeof (SolidColorBrush))) {
                FontColor.SelectedColor = ((SolidColorBrush) imp.Data.WoW.DatiCoda.FontColor).Color;
            }

            Canvas.SetTop (PosizioneTesto, imp.Data.WoW.DatiCoda.PosizioneTesto.Y);
            Canvas.SetLeft (PosizioneTesto, imp.Data.WoW.DatiCoda.PosizioneTesto.X);

            WowDotNetAPI.Region reg = (WowDotNetAPI.Region) Enum.Parse (typeof (WowDotNetAPI.Region), imp.Data.WoW.Region);
            WoW_Region.SelectedItem = reg;

            WowDotNetAPI.Locale loc = (WowDotNetAPI.Locale) Enum.Parse (typeof (WowDotNetAPI.Locale), imp.Data.WoW.Locale);
            WoW_Locale.SelectedItem = loc;

            caricato = true;
        }

        #region Impostazioni Tema

        private void Bottone_LightDark_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            if (imp.Generale.Tema == "BaseDark")
                imp.Generale.Tema = "BaseLight";
            else
                imp.Generale.Tema = "BaseDark";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Red_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Red";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void cambiaTema () {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            String tema = imp.Generale.Tema;
            String accent = imp.Generale.Accent;
            logger.InfoFormat ("Imposto tema {0} con Accent {1}", tema, accent);
            MahApps.Metro.ThemeManager.ChangeAppStyle (Application.Current, MahApps.Metro.ThemeManager.GetAccent (accent), MahApps.Metro.ThemeManager.GetAppTheme (tema));
        }

        private void Tema_Green_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Green";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Blue_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Blue";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Purple_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Purple";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Orange_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Orange";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Lime_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Lime";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Emerald_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Emerald";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Teal_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Teal";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Cyan_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Cyan";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Cobalt_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Cobalt";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Indigo_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Indigo";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Violet_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Violet";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Pink_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Pink";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Magenta_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Magenta";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Crimson_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Crimson";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Amber_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Amber";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Yellow_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Yellow";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Brown_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Brown";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Olive_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Olive";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Steel_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Steel";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Mauve_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Mauve";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Taupe_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Taupe";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        private void Tema_Sienna_Click (object sender, RoutedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Generale.Accent = "Sienna";

            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            cambiaTema ();
        }

        #endregion Impostazioni Tema

        private void ListaIntro_KeyUp (object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key != System.Windows.Input.Key.Enter)
                return;

            e.Handled = true;
            System.Windows.Input.Keyboard.ClearFocus ();
            TextBox tex = sender as TextBox;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Data.WoW.ListaIntro = tex.Text;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto lista intro su {0}", tex.Text);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void LivelloLog_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            log4net.Repository.Hierarchy.Hierarchy hier = (log4net.Repository.Hierarchy.Hierarchy) log4net.LogManager.GetRepository ();

            ComboBox cb = (ComboBox) sender;
            switch (cb.SelectedIndex) {
                case 0:
                    imp.Generale.LogLevel = 0;
                    hier.Root.Level = log4net.Core.Level.All;
                    break;

                case 1:
                    imp.Generale.LogLevel = 1;
                    hier.Root.Level = log4net.Core.Level.Debug;
                    break;

                case 2:
                    imp.Generale.LogLevel = 2;
                    hier.Root.Level = log4net.Core.Level.Info;
                    break;

                case 3:
                    imp.Generale.LogLevel = 3;
                    hier.Root.Level = log4net.Core.Level.Warn;
                    break;

                case 4:
                    imp.Generale.LogLevel = 4;
                    hier.Root.Level = log4net.Core.Level.Error;
                    break;

                case 5:
                    imp.Generale.LogLevel = 5;
                    hier.Root.Level = log4net.Core.Level.Fatal;
                    break;

                default:
                    break;
            }
            logger.InfoFormat ("Impostato livello log a {0}", hier.Root.Level);
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);

            e.Handled = true;
        }

        private void ListaMembri_KeyUp (object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key != System.Windows.Input.Key.Enter)
                return;

            e.Handled = true;
            System.Windows.Input.Keyboard.ClearFocus ();
            TextBox tex = sender as TextBox;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Data.WoW.ListaMembri = tex.Text;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto lista membri su {0}", tex.Text);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void ListaSfondo_KeyUp (object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key != System.Windows.Input.Key.Enter)
                return;

            e.Handled = true;
            System.Windows.Input.Keyboard.ClearFocus ();
            TextBox tex = sender as TextBox;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Data.WoW.ListaSfondi = tex.Text;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto lista sfondi su {0}", tex.Text);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void PesoCacheUpDown_ValueChanged (object sender, RoutedPropertyChangedEventArgs<double?> e) {
            if (caricato) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                imp.Generale.PesoCache = (PesoCacheUpDown.Value.HasValue ? PesoCacheUpDown.Value.Value : 0) * Math.Pow (1024, PesoCacheCombo.SelectedIndex);
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Peso Cache a {0}", imp.Generale.PesoCache);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void PesoCacheCombo_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (caricato) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                imp.Generale.PesoCache = (PesoCacheUpDown.Value.HasValue ? PesoCacheUpDown.Value.Value : 0) * Math.Pow (1024, PesoCacheCombo.SelectedIndex);
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Peso Cache a {0}", imp.Generale.PesoCache);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void ListaAudio_KeyUp (object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key != System.Windows.Input.Key.Enter)
                return;

            e.Handled = true;
            System.Windows.Input.Keyboard.ClearFocus ();
            TextBox tex = sender as TextBox;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Data.WoW.ListaAudio = tex.Text;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto lista audio su {0}", tex.Text);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void MaxDownload_ValueChanged (object sender, RoutedPropertyChangedEventArgs<double?> e) {
            if (caricato) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                imp.Generale.MaxDownloadSimultanei = (uint) MaxDownload.Value;
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Peso Cache a {0}", imp.Generale.PesoCache);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void Font_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (caricato) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                imp.Data.WoW.DatiCoda.Font = _fonts.ElementAt (Font.SelectedIndex).Nome;
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Font su {0}", imp.Data.WoW.DatiCoda.Font);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void FontSize_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (caricato) {
                LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
                imp.Data.WoW.DatiCoda.FontSize = _defaultFontSize.ElementAt (FontSize.SelectedIndex);
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Font Size a {0}", imp.Data.WoW.DatiCoda.FontSize);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void FontColor_SelectedColorChanged (object sender, RoutedPropertyChangedEventArgs<Color?> e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;

            if (caricato) {
                imp.Data.WoW.DatiCoda.FontColor = new System.Windows.Media.SolidColorBrush (FontColor.SelectedColor.Value);
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Colore Font su {0}", imp.Data.WoW.DatiCoda.FontColor);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }

            PosizioneTesto.Foreground = imp.Data.WoW.DatiCoda.FontColor;
        }

        private void PosizioneTesto_PreviewMouseLeftButtonDown (object sender, System.Windows.Input.MouseButtonEventArgs e) {
            isMoving = true;
            movingControl = (UIElement) sender;
            relativePosition = e.GetPosition (movingControl);
        }

        private void PosizioneTesto_PreviewMouseLeftButtonUp (object sender, System.Windows.Input.MouseButtonEventArgs e) {
            isMoving = false;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;

            Point pos = movingControl.TranslatePoint (new Point (0, 0), Preview);
            //Scalo

            imp.Data.WoW.DatiCoda.PosizioneTesto = pos;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto Posizione Testo a {0}", imp.Data.WoW.DatiCoda.PosizioneTesto);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void PosizioneTesto_PreviewMouseMove (object sender, System.Windows.Input.MouseEventArgs e) {
            if (!isMoving)
                return;

            Point canvasRelativePosition = e.GetPosition (Preview);
            Canvas.SetTop (movingControl, canvasRelativePosition.Y - relativePosition.Y);
            Canvas.SetLeft (movingControl, canvasRelativePosition.X - relativePosition.X);
        }

        private void Pattern_KeyUp (object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key != System.Windows.Input.Key.Enter)
                return;

            e.Handled = true;
            System.Windows.Input.Keyboard.ClearFocus ();
            TextBox tex = sender as TextBox;

            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;
            imp.Data.WoW.DatiCoda.PatternNome = tex.Text;
            String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
            logger.InfoFormat ("Imposto pattern nome video su {0}", tex.Text);
            System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
        }

        private void WoW_Region_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;

            if (caricato) {
                imp.Data.WoW.Region = WoW_Region.SelectedItem.ToString ();
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Region su {0}", imp.Data.WoW.Region);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }

        private void WoW_Locale_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            LegioVideoUploader.Impostazioni.Impostazioni imp = Application.Current.Properties["Impostazioni"] as LegioVideoUploader.Impostazioni.Impostazioni;

            if (caricato) {
                imp.Data.WoW.Locale = WoW_Locale.SelectedItem.ToString ();
                String json = Newtonsoft.Json.JsonConvert.SerializeObject (imp, Newtonsoft.Json.Formatting.Indented);
                logger.InfoFormat ("Imposto Locale su {0}", imp.Data.WoW.Locale);
                System.IO.File.WriteAllText (Utility.getConfigFileName (), json);
            }
        }
    }
}
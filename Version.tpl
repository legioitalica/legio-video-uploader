namespace LegioVideoUploader {

    internal class Version {
        public static readonly System.Version number = new System.Version (%Version%);
        public const string numberWithRevString = "%VersionRevString%";
        public const string numberString = "%VersionString%";
        public const string branchName = "%branchName%";
        public const string branchSha1 = "%branchSha1%";
        public const string branchRevCount = "%branchRevCount%";
        public const string informational = "%VersionRevString% [ %branchSha1% ]";
        public const string informationalFull = "%VersionRevString% [ %branchSha1% ] /'%branchName%':%branchRevCount%";
    }
}
